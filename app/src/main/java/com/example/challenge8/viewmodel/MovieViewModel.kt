package com.example.challenge8.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.example.challenge8.manager.DataStoreManager
import com.example.challenge8.model.GetMovieResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.example.challenge8.model.GetMovieResponseItem
import com.example.challenge8.network.ApiService
import kotlinx.coroutines.launch

class MovieViewModel(private val Api: ApiService, private val pref: DataStoreManager): ViewModel() {
    var movieList = MutableLiveData<List<GetMovieResponseItem>>()
    var code: Int? = null

    fun fetchAllData() {
        Api.getTopRated()
            .enqueue(object : Callback<GetMovieResponse> {
                override fun onResponse(
                    call: Call<GetMovieResponse>,
                    response: Response<GetMovieResponse>
                ) {
                    movieList.postValue(response.body()?.results)
                    code = response.code()
                }

                override fun onFailure(call: Call<GetMovieResponse>, t: Throwable) {
                    Log.e("Repository", "onFailure", t)
                }
            })
    }

    fun saveDataStore(email:String, data: String) {
        viewModelScope.launch {
            pref.setData(email, data)
        }
    }

    fun saveLoginStatus(status:String) {
        viewModelScope.launch {
            pref.setLoginStatus(status)
        }
    }

    fun getDataStore(email: String) : LiveData<String> {
        return pref.getData(email).asLiveData()
    }

    fun getLoginStatus() : LiveData<String> {
        return pref.getLoginStatus().asLiveData()
    }

}