package com.example.challenge8

import android.app.Application
import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import com.example.challenge8.manager.DataStoreManager
import com.example.challenge8.network.ApiService
import com.example.challenge8.viewmodel.MovieViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(viewModelModule, dataStoreManagerModule, apiModule, retrofitModule, chuckerModule))
        }
    }
}

val viewModelModule = module {
    single {
        MovieViewModel(get(), get())
    }
}

val dataStoreManagerModule = module {
    single {
        DataStoreManager()
    }
}

val apiModule = module {
    fun provideUseApi(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    single { provideUseApi(get()) }
}

val retrofitModule = module {
    fun provieHttpClient(interceptor: ChuckerInterceptor): OkHttpClient {
        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
        return client.build()
    }

    fun provideRetrofit(client: OkHttpClient): Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    single { provieHttpClient(get()) }
    single { provideRetrofit(get()) }
}


val chuckerModule = module {
    fun provideChuckerCollector(context: Context): ChuckerCollector {
        return ChuckerCollector(
            context = context,
            showNotification = true,
            retentionPeriod = RetentionManager.Period.ONE_WEEK)
    }
    fun provideChuckerInterceptor(context: Context, collector:ChuckerCollector): ChuckerInterceptor {
        return ChuckerInterceptor.Builder(context)
            .collector(collector)
            .maxContentLength(250_000L)
            .alwaysReadResponseBody(true)
            .build()
    }
    single { provideChuckerCollector(androidContext()) }
    single { provideChuckerInterceptor(androidContext(), get()) }
}


