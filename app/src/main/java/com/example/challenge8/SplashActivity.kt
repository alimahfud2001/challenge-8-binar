package com.example.challenge8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.example.challenge8.databinding.ActivitySplashBinding
import com.example.challenge8.manager.DataStoreManager
import com.example.challenge8.viewmodel.MovieViewModel
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity() {
    private val viewmodel: MovieViewModel by inject()
    private val pref: DataStoreManager by inject()
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        pref.setContext(this)

        var isLoggedIn = ""
        viewmodel.getLoginStatus().observe(this){
            isLoggedIn = it
        }

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        Handler().postDelayed({
            when (isLoggedIn){
                "hi" -> MainActivity.open(this, null)
                else -> MainActivity.open(this, isLoggedIn)
            }

            finish()
        }, 2000)
    }
}