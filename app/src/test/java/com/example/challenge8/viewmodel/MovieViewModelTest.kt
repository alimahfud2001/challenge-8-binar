package com.example.challenge8.viewmodel

import com.example.challenge8.network.ApiService
import com.example.challenge8.manager.DataStoreManager
import com.example.challenge8.model.GetMovieResponseItem
import io.mockk.mockk
import io.mockk.verify
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class MovieViewModelTest : TestCase() {

    private lateinit var service: ApiService
    private lateinit var viewModel: MovieViewModel
    private lateinit var datastore: DataStoreManager


    @Before
    override fun setUp() {
        service = mockk()
        datastore = DataStoreManager()
        viewModel = MovieViewModel(service, datastore)
    }


    @Test
    fun testFetchAllData(): Unit = runBlocking {
        // Mocking (GIVEN)
        val respAllCar = mockk<List<GetMovieResponseItem>>()

//        every {
//            runBlocking {
//                viewModel.fetchAllData()
//            }
//        } returns respAllCar

        // System Under Test (WHEN)
        val result = viewModel.fetchAllData()

        // (THEN)
        verify {
            runBlocking { service.getTopRated() }
        }
//        Truth.assertThat(result == respAllCar).isTrue()
    }
}